﻿using FinalTask.BLL.DTO;
using FinalTask.DAL.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinalTask.BLL.Services
{
    public interface IUserService
    {
        bool Authorize(string name,string password);

        UserDTO FindOne(int id);

        UserDTO GetUserByEmail(string user);

        Task AsyncCreate(UserDTO user);

        Task SendEmailAsync(string email, string messagebody);

        Task AsyncUpdate(UserDTO user, string password, string passwordCheck);

        Task AsyncDelete(int id);

        Task UpdateStatus(string email);


        IEnumerable<UserDTO> GetUserAll();

    }
}
