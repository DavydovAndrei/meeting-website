﻿using AutoMapper;
using FinalTask.BLL.DTO;
using FinalTask.DAL.Interfaces;
using FinalTask.DAL.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinalTask.BLL.Services
{
    public class FriendService : IFriendService
    {
        #region Fields
        private IUnitOfWork db;
        #endregion

        #region Constructor
        public FriendService(IUnitOfWork unitOfWork)
        {
            db = unitOfWork;
        }
        #endregion

        #region Methods
        public async Task AsyncAdd(FriendDTO friendDTO)
        {
            await db.Friends.AsyncCreate(Mapper.Map<Friend>(friendDTO));
            Mapper.Initialize(cfg => cfg.CreateMap<FriendDTO,Friend >()
     .ForMember(p => p.FriendsUser, opt => opt.Ignore())
     .ForMember(p => p.Users, opt => opt.Ignore()));
        }

        public async Task AsyncDelete(int id, int friendId)
          
            {
            IEnumerable<Friend> friends = db.Friends.Find(x => (x.UserId == id && x.UserFriendsId == friendId || x.UserId == friendId && x.UserFriendsId == id));
            foreach (var friend in friends)
                await db.Friends.AsyncDelete(friend.Id);
                
            }
        

        public IEnumerable<FriendDTO> GetFriendsAll(int id)
        {   
            List<FriendDTO> result = new List<FriendDTO>();

            foreach (var item in db.Friends.Find(x=>x.UserId==id))
                result.Add(Mapper.Map<FriendDTO>(item));

            return result;
        }
        #endregion
    }
}
