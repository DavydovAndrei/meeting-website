﻿using AutoMapper;
using FinalTask.BLL.DTO;
using FinalTask.DAL.Interfaces;
using FinalTask.DAL.Model;
using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinalTask.BLL.Services
{
    public class UserService : IUserService
    {
        #region Fields
        private IUnitOfWork db;
        #endregion

        #region Constructor
        public UserService(IUnitOfWork unitOfWork)
        {
            db = unitOfWork;
        }
        #endregion

        #region Methods

        public UserDTO FindOne(int id)
        {
            var result = db.Users.Get(id);
            return Mapper.Map<UserDTO>(result);
        }

        public async Task UpdateStatus(string email)
        {
            var user = db.Users.FindOne(x => x.Email == email);
            user.LastVisitTime = DateTime.Now;
            await db.Users.AsyncUpdate(user);
        }

        public UserDTO GetUserByEmail(string email)
        {
            var result = db.Users.FindOne(x => x.Email.Contains(email));

            Mapper.Reset();

            Mapper.Initialize(cfg => cfg.CreateMap<User, UserDTO>()
                .ForMember(p => p.Password, opt => opt.Ignore()));
            return Mapper.Map<UserDTO>(result);
        }

        public bool Authorize(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return false;

            var user = db.Users.FindOne(x => x.Email == username && VerifyPasswordHash(password, x.PasswordHash, x.PasswordSalt));

            if (user != null)
            {
                return true;
            }
            return false;
        }

        public async Task SendEmailAsync(string email,string messagebody)
        {
            string messageBody = null;
            string messageSubject = null;
            
            
               
                messageSubject = "Site MeetMe";
            

            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Registration ", "a.davydov.vironit@vironit.ru"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = messageSubject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = messageBody
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.yandex.ru", 25, false);
                await client.AuthenticateAsync("a.davydov.vironit@vironit.ru", "15062018dad");
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }

        public async Task AsyncCreate(UserDTO userDTO)
        {
            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(userDTO.Password, out passwordHash, out passwordSalt);

            userDTO.PasswordHash = passwordHash;
            userDTO.PasswordSalt = passwordSalt;

            await db.Users.AsyncCreate(Mapper.Map<User>(userDTO));

            await db.Save();

            await SendEmailAsync(userDTO.Email, "<a href=\"http://localhost:3459/Home/Login" + "\">Сlick on link to complete registration</a> ");
        }

        public IEnumerable<UserDTO> GetUserAll()
        {
            List<UserDTO> result = new List<UserDTO>();

            foreach (var item in db.Users.GetAll())
                result.Add(Mapper.Map<UserDTO>(item));

            return result;
        }

        public async Task AsyncDelete(int id)
        {
            await db.Users.AsyncDelete(id);
            await db.Save();
        }

        public async Task AsyncUpdate(UserDTO userParam, string password, string passwordCheck)
        {
            var user = db.Users.FindOne(x => x.Email == userParam.Email);

            if (!string.IsNullOrWhiteSpace(password) && VerifyPasswordHash(passwordCheck, user.PasswordHash, user.PasswordSalt))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
                user.Password = password;
                await SendEmailAsync(user.Email, "You password changed");

            }
         
            
                user.Name = userParam.Name;
                user.Height = userParam.Height;
                user.Weight = userParam.Weight;
                user.Interests = userParam.Interests;
                user.Date = userParam.Date;
                user.Path = userParam.Path;
                user.Gender = userParam.Gender;
                user.MaritalStatus = userParam.MaritalStatus;
                user.Education = userParam.Education;
                user.Nationality = userParam.Nationality;
                user.ZodiacSign = userParam.ZodiacSign;
                user.Languages = userParam.Languages;
                user.BadHabits = userParam.BadHabits;
                user.Financial = userParam.Financial;


            await db.Users.AsyncUpdate(user);
            await SendEmailAsync(user.Email, "You profile changed");
            await db.Save();
            
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
        #endregion
    }
}
