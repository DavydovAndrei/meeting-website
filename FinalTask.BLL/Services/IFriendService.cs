﻿using FinalTask.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalTask.BLL.Services
{
    public interface IFriendService
    {
        Task AsyncAdd(FriendDTO friend);

        IEnumerable<FriendDTO> GetFriendsAll(int id);

        Task AsyncDelete(int id,int friendid);





    }
}
