﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalTask.BLL.DTO
{
   public class FriendDTO
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int UserFriendsId { get; set; }
    }
}
