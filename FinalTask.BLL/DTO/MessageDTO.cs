﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalTask.BLL.DTO
{
    class MessageDTO
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public int FromId { get; set; }

        public int ToId { get; set; }

        public DateTime TimeMessage { get; set; }
    }
}
