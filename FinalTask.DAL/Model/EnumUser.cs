﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FinalTask.DAL.Model
{
     public class EnumUser
    {
        public enum Gender
        {
            [Display(Name = "Male")]
            Male = 1,
            [Display(Name = "Female")]
            Female = 2
        }

        public enum PurposeOfDating
        {
            [Display(Name = "Sex")]
            Sex = 1,
            [Display(Name = "Meet")]
            Meet = 2,
            [Display(Name = "Family")]
            Family =3,
            [Display(Name = "Сommunication")]
            Сommunication =4
        }

        public enum MaritalStatus
        {
            [Display(Name = "Single")]
            Single = 1,
            [Display(Name = "Relations")]
            Relations = 2,
            [Display(Name = "Married")]
            Married =3,
            [Display(Name = "Complicated")]
            Complicated =4

        }

        public enum Education
        {
            [Display(Name = "BasicEducation")]
            BasicEducation = 1,
            [Display(Name = "SecondaryEducation")]
            SecondaryEducation = 2,
            [Display(Name = "HigherEducation")]
            HigherEducation =3

        }
        public enum Nationality
        {
            [Display(Name = "Austrians")]
            Austrians = 1,
            [Display(Name = "Azerbaijanis")]
            Azerbaijanis = 2,
            [Display(Name = "African")]
            African = 3,
            [Display(Name = "Arabs")]
            Arabs = 4,
            [Display(Name = "Armenians")]
            Armenians = 5,
            [Display(Name = "Belarusians")]
            Belarusians = 6,
            [Display(Name = "Jews")]
            Jews = 7,
            [Display(Name = "Chinese")]
            Chinese = 8,
            [Display(Name = "Russian")]
            Russian = 9,
            [Display(Name = "American")]
            American = 10,
            [Display(Name = "Englishman")]
            Englishman = 11,
            [Display(Name = "Frenchman")]
            Frenchman = 12,
            [Display(Name = "German")]
            German = 13
            
        }
        public enum ZodiacSign
        {
            [Display(Name = "Aries")]
            Aries =1,
            [Display(Name = "Taurus")]
            Taurus =2,
            [Display(Name = "Gemini")]
            Gemini =3,
            [Display(Name = "Cancer")]
            Cancer =4,
            [Display(Name = "Leo")]
            Leo =5,
            [Display(Name = "Virgo")]
            Virgo =6,
            [Display(Name = "Libra")]
            Libra =7,
            [Display(Name = "Scorpio")]
            Scorpio =8,
            [Display(Name = "Sagittarius")]
            Sagittarius =9,
            [Display(Name = "Capricorn")]
            Capricorn =10,
            [Display(Name = "Aquarius")]
            Aquarius =11,
            [Display(Name = "Pisces")]
            Pisces =12
        }

        public enum Languages
        {
            [Display(Name = "English")]
            English =1,
            [Display(Name = "German")]
            German =2,
            [Display(Name = "French")]
            French =3,
            [Display(Name = "Chinese")]
            Chinese =4
        }

        public enum BadHabits
        {
            [Display(Name = "Smoking")]
            Smoking =1,
            [Display(Name = "Alcohol")]
            Alcohol =2,
            [Display(Name = "Drugs")]
            Drugs =3
        }
        public enum Financial
        {
            [Display(Name = "Rich")]
            Rich =1,
            [Display(Name = "Poor")]
            Poor =2,
            [Display(Name = "AverageProsperity")]
            AverageProsperity =3
        }


    }

}
