﻿

using FinalTask.DAL.Model;
using Microsoft.EntityFrameworkCore;

namespace Firstcore.DLL.Model
{
        public class UserContext : DbContext
        {
        public UserContext(DbContextOptions<UserContext> options)
                : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Friend> Friends { get; set; }
        public DbSet<Message> Messages { get; set; }

    }
    }


