﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalTask.DAL.Model
{
   public class Friend
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int UserFriendsId { get; set; }

        public virtual User Users { get; set; }

        public virtual User FriendsUser{ get; set; }
    }
}
