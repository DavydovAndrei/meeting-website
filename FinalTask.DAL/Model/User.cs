﻿using System;
using System.Collections.Generic;
using System.Text;
using static FinalTask.DAL.Model.EnumUser;

namespace FinalTask.DAL.Model
{
    public class User
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public int Height { get; set; }

        public int Weight { get; set; }

        public string Interests { get; set; }

        public DateTime Date { get; set; }

        public bool Online { get; set; } = false;

        public string Password { get; set; }
      
        public byte[] PasswordHash { get; set; }
      
        public byte[] PasswordSalt { get; set; }

        public string Path { get; set; }

        public DateTime LastVisitTime { get; set; }

        //ENUM ID
        public Gender Gender { get; set; }

        public PurposeOfDating PurposeOfDating { get; set; }

        public MaritalStatus MaritalStatus { get; set; }

        public Education Education { get; set; }

        public Nationality Nationality { get; set; }

        public ZodiacSign ZodiacSign { get; set; }

        public Languages Languages { get; set; }

        public BadHabits BadHabits { get; set; }

        public Financial Financial { get; set; }


    }
}
