﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalTask.DAL.Model
{
    public class Message
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public int FromId { get; set;}

        public int ToId { get; set; }

        public DateTime TimeMessage { get; set; }

        public virtual User Users { get; set; }
    }
}
