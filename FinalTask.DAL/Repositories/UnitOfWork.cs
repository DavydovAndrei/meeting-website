﻿using FinalTask.DAL.Interfaces;
using FinalTask.DAL.Model;
using Firstcore.DLL.Model;
using System;
using System.Threading.Tasks;

namespace FinalTask.DAL.Repositories
{
   public class UnitOfWork : IUnitOfWork
    {
        private UserContext db;

        private UserRepository userRepository;

        private FriendRepository friendRepository;

        private MessageRepository messageRepository;

        public UnitOfWork(UserContext context)
        {
            db = context;

        }
        public IRepository<User> Users
        {
            get
            {
                if (userRepository == null)
                    userRepository = new UserRepository(db);
                return userRepository;
            }
        }

        public IRepository<Message> Messages
        {
            get
            {
                if (messageRepository == null)
                    messageRepository = new MessageRepository(db);
                return messageRepository;
            }
        }

        public IRepository<Friend> Friends
        {
            get
            {
                if (friendRepository == null)
                    friendRepository = new FriendRepository(db);
                return friendRepository;
            }
        }


        public async Task Save()
        {
            await db.SaveChangesAsync();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

