﻿using FinalTask.DAL.Interfaces;
using FinalTask.DAL.Model;
using Firstcore.DLL.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalTask.DAL.Repositories
{
   public class FriendRepository:IRepository<Friend>
    {
        private UserContext db;

        public FriendRepository(UserContext context)
        {
            this.db = context;
        }

        public IEnumerable<Friend> GetAll()
        {
            return db.Friends;
        }

        public Friend Get(int id)
        {
            return db.Friends.Find(id);
        }

        public async Task AsyncCreate(Friend friend)
        {
            db.Friends.Add(friend);
            await db.SaveChangesAsync();
        }

        public async Task AsyncUpdate(Friend friend)
        {
            db.Entry(friend).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public IEnumerable<Friend> Find(Func<Friend, Boolean> predicate)
        {
            return db.Friends.Where(predicate).ToList();
        }

        public Friend FindOne(Func<Friend, Boolean> predicate)
        {
            return db.Friends.SingleOrDefault(predicate);

        }

        public async Task AsyncDelete(int id)
        {
            Friend friend = db.Friends.Find(id);
            if (friend != null)
                db.Friends.Remove(friend);
            await db.SaveChangesAsync();
        }
    }
}
   
