﻿using FinalTask.DAL.Interfaces;
using FinalTask.DAL.Model;
using Firstcore.DLL.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalTask.DAL.Repositories
{
    public class MessageRepository:IRepository<Message>
    {
        private UserContext db;

    public MessageRepository(UserContext context)
    {
        this.db = context;
    }

    public IEnumerable<Message> GetAll()
    {
        return db.Messages;
    }

    public Message Get(int id)
    {
        return db.Messages.Find(id);
    }

    public async Task AsyncCreate(Message message)
    {
        db.Messages.Add(message);
        await db.SaveChangesAsync();
    }

    public async Task AsyncUpdate(Message message)
    {
        db.Entry(message).State = EntityState.Modified;
        await db.SaveChangesAsync();
    }

    public IEnumerable<Message> Find(Func<Message, Boolean> predicate)
    {
        return db.Messages.Where(predicate).ToList();
    }

    public Message FindOne(Func<Message, Boolean> predicate)
    {
        return db.Messages.SingleOrDefault(predicate);

    }

    public async Task AsyncDelete(int id)
    {
         Message message = db.Messages.Find(id);
        if (message != null)
            db.Messages.Remove(message);
        await db.SaveChangesAsync();
    }
}
}
