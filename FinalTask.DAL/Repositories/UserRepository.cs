﻿using FinalTask.DAL.Interfaces;
using FinalTask.DAL.Model;
using Firstcore.DLL.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalTask.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private UserContext db;

        public UserRepository(UserContext context)
        {
            this.db = context;
        }

        public IEnumerable<User> GetAll()
        {
            return db.Users;
        }

        public User Get(int id)
        {
            return db.Users.Find(id);
        }

        public async Task AsyncCreate(User user)
        {
            db.Users.Add(user);
            await db.SaveChangesAsync();
        }

        public async Task AsyncUpdate(User user)
        {
            db.Entry(user).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public IEnumerable<User> Find(Func<User, Boolean> predicate)
        {
            return db.Users.Where(predicate).ToList();
        }

        public User FindOne (Func<User, Boolean> predicate)
        {
            return db.Users.SingleOrDefault(predicate);
        
        }

        public async Task AsyncDelete(int id)
        {
            User user = db.Users.Find(id);
            if (user != null)
                db.Users.Remove(user);
            await db.SaveChangesAsync();
        }
    }
}

