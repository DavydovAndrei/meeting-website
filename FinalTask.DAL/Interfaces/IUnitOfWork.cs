﻿using FinalTask.DAL.Model;
using System;
using System.Threading.Tasks;

namespace FinalTask.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<User> Users { get; }
        IRepository<Friend> Friends { get; }
        Task Save();
    }
}
