﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinalTask.DAL.Interfaces
{
    public interface IRepository<T> where T : class
        {
            IEnumerable<T> GetAll();
            T Get(int id);
            IEnumerable<T> Find(Func<T, Boolean> predicate);
            T FindOne(Func<T, Boolean> predicate);
            Task AsyncCreate(T item);
            Task AsyncUpdate(T item);
            Task AsyncDelete(int id);
        }
}

