﻿using AutoMapper;
using FinalTask.BLL.DTO;
using FinalTask.BLL.Services;
using FinalTask.WEB.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FinalTask.Controllers
{
    [AllowAnonymous] 
    public class HomeController : Controller
    {
        #region Fields
        IUserService UserService;

        #endregion


        #region Constructor
        public HomeController(IUserService serv)
        {
            UserService = serv;
        }
        #endregion


        #region Methods
        public IActionResult Index()
        {

            return View();
        }
        
        public IActionResult CheckEmail()
        {

            return View();
        }

        [HttpGet]
        public IActionResult Registration()
        {

            return View();

        }


        [HttpPost]
        public async Task<IActionResult> Registration(RegistrationViewModel registerModel)
        {
           if(ModelState.IsValid)
            {
                Mapper.Reset();
                Mapper.Initialize(cfg => cfg.CreateMap<RegistrationViewModel, UserDTO>()
                .ForMember(p => p.PasswordHash, opt => opt.Ignore())
                .ForMember(p => p.PasswordSalt, opt => opt.Ignore()));
                UserDTO user = Mapper.Map<RegistrationViewModel, UserDTO>(registerModel);
                await UserService.AsyncCreate(user);
               
                return RedirectToAction("CheckEmail","Home");

               
            }
            return View();
            
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Forgotpassword()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Forgotpassword(LoginViewModel checkModel)
        {
            UserDTO userDTO = UserService.GetUserByEmail(checkModel.Email);
            if (userDTO != null)
            {
                UserService.SendEmailAsync(checkModel.Email, userDTO.Password);
                return RedirectToAction("CheckEmail", "Home");
            }
            else
            {
                
                return View();
            }
        }

        public async Task <IActionResult> Logout()
        {

            

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login");
        }


        #endregion

    }
}
