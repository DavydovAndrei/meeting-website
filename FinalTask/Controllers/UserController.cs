﻿using AutoMapper;
using FinalTask.BLL.DTO;
using FinalTask.BLL.Services;
using FinalTask.WEB.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FinalTask.WEB.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        #region Fields
        private IUserService UserService;
        IHostingEnvironment _appEnvironment;
        #endregion

        #region Constructor
        public UserController(IUserService serv, IHostingEnvironment appEnvironment)
        {
            UserService = serv;
            _appEnvironment = appEnvironment;
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Profile()
        {
            UserDTO user = UserService.GetUserByEmail(User.Identity.Name);
            if (User.Identity.IsAuthenticated)
            {
                user.Online = true;

            }


            Mapper.Reset();
            Mapper.Initialize(cfg => cfg.CreateMap<UserDTO, UserProfileViewModel>()
            .ForMember(p => p.PasswordHash, opt => opt.Ignore())
            .ForMember(p => p.PasswordSalt, opt => opt.Ignore()));


            var userSave = Mapper.Map<UserProfileViewModel>(user);


            return View(userSave);
        }

        [HttpGet]
        public async Task<IActionResult> Search()
        {
            IEnumerable<UserDTO> UserDTOs = UserService.GetUserAll();

            Mapper.Reset();
            Mapper.Initialize(cfg => cfg.CreateMap<UserDTO, UserProfileViewModel>()
            .ForMember(p => p.PasswordHash, opt => opt.Ignore())
            .ForMember(p => p.PasswordSalt, opt => opt.Ignore()));

            SearchViewModel SearchUser=new SearchViewModel();
           SearchUser.UserProfile = Mapper.Map<List<UserProfileViewModel>>(UserDTOs);
           

            return View(SearchUser);
        }

        [HttpPost]
        public async Task<IActionResult> Search(SearchViewModel SearchUser)
        {
            
            IEnumerable<UserDTO> UserDTOs = UserService.GetUserAll().Where(c => c.Gender == SearchUser.Gender
            && c.Nationality == SearchUser.Nationality
            && c.ZodiacSign == SearchUser.ZodiacSign
            && c.Languages == SearchUser.Languages
            && c.BadHabits == SearchUser.BadHabits
            && c.Financial == SearchUser.Financial
            && c.Education==SearchUser.Education
            && c.MaritalStatus==SearchUser.MaritalStatus
            && c.PurposeOfDating==SearchUser.PurposeOfDating
            && ((DateTime.Now.Year) -(c.Date.Year)) >= SearchUser.FromAge
            && ((DateTime.Now.Year) - (c.Date.Year)) <= SearchUser.ToAge
            );
            Mapper.Reset();
            Mapper.Initialize(cfg => cfg.CreateMap<UserDTO, UserProfileViewModel>()
            .ForMember(p => p.PasswordHash, opt => opt.Ignore())
            .ForMember(p => p.PasswordSalt, opt => opt.Ignore()));

            SearchViewModel UserSearch = new SearchViewModel();
            UserSearch.UserProfile = Mapper.Map<List<UserProfileViewModel>>(UserDTOs);

            return View(UserSearch);
        }

        [HttpPost]
        public async Task UpdateStatus()
        {
            var email = User.Identity.Name;
            await UserService.UpdateStatus(email);
        }

        [HttpPost]
        public async Task<IActionResult> EditProfile(UserProfileViewModel userEdit)
        {
            
            UserDTO user = UserService.GetUserByEmail(User.Identity.Name);
            string path ="" ;
            if (userEdit.Avatar != null)
            {
                // path to folder  Files
                path = "/images/" + userEdit.Avatar.FileName;
                // save file in folder Files in catalog wwwroot
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await userEdit.Avatar.CopyToAsync(fileStream);
                }
                userEdit.Path = path;
            }

            else
            {
                userEdit.Path = user.Path;
            }

            Mapper.Reset();

            Mapper.Initialize(cfg => cfg.CreateMap<UserProfileViewModel, UserDTO>()
                .ForMember(p => p.PasswordHash, opt => opt.Ignore())
                .ForMember(p => p.PasswordSalt, opt => opt.Ignore()));
                
                

            UserDTO userE = Mapper.Map<UserProfileViewModel, UserDTO>(userEdit);


            

            await UserService.AsyncUpdate(userE, userEdit.Password, userEdit.OldPassword);

            return RedirectToAction("Profile");
            #endregion

        }
    }
}







