﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalTask.WEB.Chat
{
    public class MessageDetail
    {
        public int FromId { get; set; }

        public string FromUserName { get; set; }

        public int ToId { get; set; }

        public string ToUserName { get; set; }

        public string Message { get; set; }
    }
}
