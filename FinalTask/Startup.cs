﻿using AutoMapper;
using FinalTask.BLL.Services;
using FinalTask.DAL.Interfaces;
using FinalTask.DAL.Repositories;
using Firstcore.DLL.Model;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using SimpleTokenProvider.Test;
using SimpleTokenProvider.Test.TokenProvider;
using System;
using System.Text;
using System.Threading.Tasks;

namespace FinalTask
{
    public class Startup
    {
        private static readonly string secretKey = "mysupersecret_secretkey!123";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            {
                var connection = Configuration["ConnectionStrings:DefaultConnection"];
                services.AddDbContextPool<UserContext>(
                    options => options.UseMySql(connection,
                    mySqlOptions =>
                    {
                        mySqlOptions.ServerVersion(new Version(8, 0, 12), Pomelo.EntityFrameworkCore.MySql.Infrastructure.ServerType.MySql);
                    }
                    ));
            }
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddSignalR();

            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));

            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,

                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = "ExampleIssuer",

                
                ValidateAudience = true,
                ValidAudience = "ExampleAudience",

                
                ValidateLifetime = true,

                
                ClockSkew = TimeSpan.Zero
            };
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = tokenValidationParameters;
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
             .AddCookie(options =>
             {
                 options.Events.OnRedirectToLogin = context =>
                 {
                     context.Response.Redirect("/Home/Login");
                     return Task.CompletedTask;
                 };

                 options.Cookie.Name = "access_token";
                 options.TicketDataFormat = new CustomJwtDataFormat(
                     SecurityAlgorithms.HmacSha256,
                     tokenValidationParameters);
             });
            services.AddMvc();
            Mapper.Initialize(cfq => { });
        }

        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            ConfigureAuth(app);

            app.UseMvc(routes =>
            {
            routes.MapRoute(
                name: "default",
                template: "{controller}/{action}/{id?}",
             defaults: new { controller = "Home", action = "Index" });
               
            });
        }
        private void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseSimpleTokenProvider(new TokenProviderOptions
            {
                Path = "/Home/Authentication",
                Audience = "ExampleAudience",
                Issuer = "ExampleIssuer",
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey)), SecurityAlgorithms.HmacSha256),
                IdentityResolver = GetIdentity,
            });
            app.UseAuthentication();
        }

        private  Task<bool> GetIdentity(string username, string password, IUserService userService)


        {
            return Task.FromResult (userService.Authorize(username, password));
        }

    }     
            

            
}
    
    

