﻿using FinalTask.WEB.Chat;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Linq;

namespace FinalTask.WEB
{
    public class ChatHub : Hub
    {
        static List<UserDetail> ConnectedUsers = new List<UserDetail>();
        static List<MessageDetail> CurrendMessage = new List<MessageDetail>();

        public async void Connect(string userName, int userId)
        {
            var id = Context.ConnectionId;

            if (ConnectedUsers.Count(x => x.ConnectionId == id) == 0)
            {
                ConnectedUsers.Add(new UserDetail { ConnectionId = id, UserName = userName, UserId = userId });
                UserDetail CurrentUser = ConnectedUsers.Where(u => u.ConnectionId == id).FirstOrDefault();
                await Clients.Caller.SendAsync("onConnected", CurrentUser.UserId.ToString(), CurrentUser.)
                    await Clients.AllExcept(CurrentUser.ConnectionId).SendAsync("onNewUserConnected", CurrentUser)


            }
        }


        public async void SendPrivateMessage(string toUserId, string message)

        {

            try
            {
                string fromconnectionid = Context.ConnectionId;
                string strFromUserId = (ConnectedUsers.Where(u => u.ConnectionId == Context.ConnectionId)
                    .Select(u => u.UserId).FirstOrDefault()).ToString();
                int _fromUserId = 0;
                int.TryParse(strFromUserId, out _fromUserId);
                int _toUserId = 0;
                int.TryParse(toUserId, out _toUserId);
                List<UserDetail> FromUsers = ConnectedUsers
                    .Where(u => u.UserId == _fromUserId).ToList();
                List<UserDetail> ToUsers = ConnectedUsers
                    .Where(u => u.UserId == _toUserId).ToList();

                if (FromUsers.Count != 0 && ToUsers.Count() != 0)
                {
                    foreach (var ToUser in ToUsers)
                    {
                        await Clients.Client(ToUser.ConnectionId)
                            .SendCoreAsync("SendPrivateMessage", new object[]
                            { _fromUserId.ToString(),FromUsers[0].UserName,message };
                    }
                }

                foreach (var FromUser in FromUsers)
                {
                    // send to caller user                                                                                //Chat Title
                    Clients.Client(FromUser.ConnectionId).sendPrivateMessage(_toUserId.ToString(), FromUsers[0].UserName, ToUsers[0].UserName, message);
                }
                // send to caller user
                //Clients.Caller.sendPrivateMessage(_toUserId.ToString(), FromUsers[0].UserName, message);
                //ChatDB.Instance.SaveChatHistory(_fromUserId, _toUserId, message);
                MessageDetail _MessageDeail = new MessageDetail { FromUserID = _fromUserId, FromUserName = FromUsers[0].UserName, ToUserID = _toUserId, ToUserName = ToUsers[0].UserName, Message = message };
                AddMessageinCache(_MessageDeail);





            }


        }

    }

