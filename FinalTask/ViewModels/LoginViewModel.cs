﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FinalTask.WEB.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Email not entered")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password not entered")]
        public string Password { get; set; }
    }
}
