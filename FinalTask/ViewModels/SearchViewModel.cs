﻿using FinalTask.BLL.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static FinalTask.DAL.Model.EnumUser;

namespace FinalTask.WEB.ViewModels
{
 // статус online/offline
    public class SearchViewModel
    {
        public IEnumerable<UserProfileViewModel> UserProfile;

        [Range(1, 110, ErrorMessage = "Age incorrect")]
        public int FromAge { get; set; }

        [Range(1, 110, ErrorMessage = "Age incorrect")]
        public int ToAge { get; set; }

        public Gender Gender { set; get; }

        public PurposeOfDating PurposeOfDating { get; set; }

        public MaritalStatus MaritalStatus { get; set; }

        public Education Education { get; set; }

        public Nationality Nationality { get; set; }

        public ZodiacSign ZodiacSign { get; set; }

        public Languages Languages { get; set; }

        public BadHabits BadHabits { get; set; }

        public Financial Financial { get; set; }
        

    }
}
