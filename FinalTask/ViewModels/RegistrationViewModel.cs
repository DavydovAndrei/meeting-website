﻿using System;
using System.ComponentModel.DataAnnotations;
using static FinalTask.DAL.Model.EnumUser;

namespace FinalTask.WEB.ViewModels
{
    public class RegistrationViewModel
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Email not entered")]
        [EmailAddress(ErrorMessage = "Email incorrect")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Name not entered")]
        [StringLength(15, MinimumLength = 2, ErrorMessage = "The length of the string must be between 2 and 15 characters")]
        public string Name { get; set; }
        

        [Required]
        [Range(10, 300, ErrorMessage = "Height incorrect")]
        public int Height { get; set; }

        [Required]
        [Range(10, 500, ErrorMessage = "Weight incorrect")]
        public int Weight { get; set; }

        public string Interests { get; set; }

        [Required(ErrorMessage = "Date not entered")]
        public DateTime Date { get; set; }

        public bool Online { get; set; } = false;

        [Required(ErrorMessage = "Password not entered")]
        public string Password { get; set; }

        public byte[] PasswordHash { get; set; }

        public byte[] PasswordSalt { get; set; }


        //ENUM ID
        public Gender Gender { get; set; }

        public PurposeOfDating PurposeOfDating { get; set; }

        public MaritalStatus MaritalStatus { get; set; }

        public Education Education { get; set; }

        public Nationality Nationality { get; set; }

        public ZodiacSign ZodiacSign { get; set; }

        public Languages Languages { get; set; }

        public BadHabits BadHabits { get; set; }

        public Financial Financial { get; set; }
    }
}



